set_property PACKAGE_PIN G10 [get_ports clk_125_p]
set_property PACKAGE_PIN F10 [get_ports clk_125_n]
set_property IOSTANDARD LVDS [get_ports clk_125_*]

#In the gth IP core a banck different from the 226 is setted, here i'm using the 226 and this constrains override that configuration.
#The recFclk has to be the same.
#set_property PACKAGE_PIN R4   [get_ports "gt_tx_p"    ] ;#  SMA_MGT_TX_P
#set_property PACKAGE_PIN P2   [get_ports "gt_rx_p"    ] ;#  SMA_MGT_RX_C_P
#set_property PACKAGE_PIN P1   [get_ports "gt_rx_n"    ] ;#  SMA_MGT_RX_C_N
#set_property PACKAGE_PIN R3   [get_ports "gt_tx_n"    ] ;#  SMA_MGT_TX_N

set_property PACKAGE_PIN V2 [get_ports gt_rx_p]
set_property PACKAGE_PIN V1 [get_ports gt_rx_n]
set_property PACKAGE_PIN W4 [get_ports gt_tx_p]
set_property PACKAGE_PIN W3 [get_ports gt_tx_n]

set_property PACKAGE_PIN V5 [get_ports gt_refclk_n]
set_property PACKAGE_PIN V6 [get_ports gt_refclk_p]

#set_property PACKAGE_PIN H27  [get_ports "rxUserClk_p"] ;#  USER_SMA_GPIO_P
#set_property PACKAGE_PIN G27  [get_ports "rxUserClk_n"] ;#  USER_SMA_GPIO_N
#set_property IOSTANDARD  LVDS [get_ports "rxUserClk_*"] ;#  does not support LVDS, only LVCMOS18

#set_property PACKAGE_PIN H27 [get_ports txUserClk_p]
#set_property PACKAGE_PIN G27 [get_ports txUserClk_n]
#set_property IOSTANDARD SUB_LVDS [get_ports txUserClk_*]
set_property PACKAGE_PIN H27 [get_ports rxUserClk_1] ;# <- best way to see rxusrclk
set_property PACKAGE_PIN G27 [get_ports rxUserClk_2]
#set_property PACKAGE_PIN G27 [get_ports txUserClk]
set_property IOSTANDARD LVCMOS18 [get_ports rxUserClk_*] ;# <-
set_property SLEW FAST [get_ports rxUserClk_*] ;# <-
set_property DRIVE 16 [get_ports rxUserClk_*] ;# <-

#J6 jumper is connected (like short circuit) to ground, so SFP1 disable is at ground, in my board (Xilinx Ultrascale kcu105).
#set_property PACKAGE_PIN D28 [get_ports "SFP1_enable"]; # necessary for using fibre on SFP ports as data channel between Tx and Rx
#set_property IOSTANDARD LVCMOS18 [get_ports "SFP1_enable"];

set_property RXSLIDE_MODE PMA [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]
#get_property RXSLIDE_MODE [get_cells -hierarchical -filter {NAME =~ *gt_inst*GT*E3_CHANNEL_PRIM_INST}]

create_clock -period 8.000 -name clk_125 -add [get_ports clk_125_p]

create_clock -period 4.166 -name gt_refclk -add [get_ports gt_refclk_p]

