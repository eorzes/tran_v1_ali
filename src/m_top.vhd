library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
--use work.utility_pkg.all;
use work.gt_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity m_top is
  port(
    clk_125_p   : in  std_logic;
    clk_125_n   : in  std_logic;
    gt_refclk_p : in  std_logic;
    gt_refclk_n : in  std_logic;
    gt_rx_p     : in  std_logic;
    gt_rx_n     : in  std_logic;
    gt_tx_p     : out std_logic;
    gt_tx_n     : out std_logic
  );
end m_top;

architecture top_level of m_top is
  
  signal clk_sys       : std_logic;
  signal gen_rst       : std_logic;                     -- from external controller (e.g. VIO), must be synced with gt_tx_usrclk
  signal gen_data      : std_logic_vector(31 downto 0); 
  signal gen_ctrl      : std_logic_vector( 3 downto 0); 
  
  signal enc_bypass    : std_logic;                     -- from external controller (e.g. VIO), must be synced with gt_tx_usrclk
  signal disp_from_enc : std_logic;  
  signal disp_to_enc   : std_logic;  
  
  signal gt_ctrl       : gt_ctrl_t;
  signal gt_stat       : gt_stat_t;
  signal to_gt         : to_gt_t;
  signal from_gt       : from_gt_t;
  
begin
  
  clk_inst: entity work.clk_buf
  port map
  (
    i     => clk_125_p,  
    ib    => clk_125_n,  
    o     => clk_sys
  );
  
  gen_inst: entity work.data_generator
    port map(
      rst_i   => gen_rst, -- from VIO
      clk_i   => from_gt.tx_usrclk,
      data_o  => gen_data,
      ctrl_o  => gen_ctrl
    );
  
  --prbs_inst: 
  
  
  enc_inst : entity work.multibyte_enc8b10b
    generic map(NBR => 4)
    port map(
      clk_i         => from_gt.tx_usrclk,
      ctrl_i        => gen_ctrl,
      data_i        => gen_data,
      rundp_o       => disp_from_enc,
      rundp_i       => disp_to_enc,
      bypass_en_i   => enc_bypass,
      bypass_data_i => x"FFFFF00000",
      data_o        => to_gt.tx_data
    );
      disp_to_enc <= disp_from_enc;

  m_gt_inst: entity work.m_gt
    generic map(N_CH => 1, MTX => 0)
    port map(
      clk_sys      => clk_sys,
      gt_refclk_p  => gt_refclk_p,
      gt_refclk_n  => gt_refclk_n,
      gt_rx_p(0)   => gt_rx_p,  
      gt_rx_n(0)   => gt_rx_n,  
      gt_tx_p(0)   => gt_tx_p,  
      gt_tx_n(0)   => gt_tx_n,  
      gt_ctrl_i(0) => gt_ctrl, --> record containing signals that could be driven  by an external controller  
      gt_stat_o(0) => gt_stat, --> record containing signals that could be checked by an external controller
      gt_i(0)      => to_gt,   --> record containing signals from the user logic to the GT 
      gt_o(0)      => from_gt  --> record containing signals from the GT to the user logic
    );

  -------------------------------------
  -- DEBUGGING TOOLS BELOW
  -------------------------------------
  
  vio_tx_inst : entity work.vio_0
    port map(
      clk           => from_gt.tx_usrclk,
      probe_out0(0) => gen_rst, -- default 1 => therefore generator not running at start-up
      probe_out1(0) => enc_bypass  
    );

  vio_rx_inst : entity work.vio_0
      port map(
        clk           => from_gt.rx_usrclk,
        probe_out0(0) => gt_ctrl.rxcommadeten,
        probe_out1    => open,
        probe_out2    => open          
      );  
  
  --prbs_check_inst: 
        
  ila_rx_inst : entity work.ila_0
    port map(
      clk    => from_gt.rx_usrclk,
      probe0 => from_gt.rx_data,
      probe1 => from_gt.rx_ctrl0,-- rxbyte is a K character
      probe2(0) => from_gt.rx_rdy
  );
        
end architecture;
