library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;
use work.gt_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity m_top is
  port(
    clk_125_p   : in  std_logic;
    clk_125_n   : in  std_logic;
    gt_refclk_p : in  std_logic;
    gt_refclk_n : in  std_logic;
    
    gt_rx_p     : in  std_logic;
    gt_rx_n     : in  std_logic;
    gt_tx_p     : out std_logic;
    gt_tx_n     : out std_logic;
    
    --rxUserClk_p : out std_logic;
    --rxUserClk_n : out std_logic
    --txUserClk_p : out std_logic;
    --txUserClk_n : out std_logic;
    rxUserClk_1   : out std_logic;
    rxUserClk_2   : out std_logic
    --SFP1_enable: out std_logic
    
  );
end m_top;

architecture rtl of m_top is
  
  signal clk_sys       : std_logic;
  signal gen_rst       : std_logic; -- from external controller (e.g. VIO), must be synced with gt_tx_usrclk
  signal gen_data      : std_logic_vector(31 downto 0); 
  signal gen_ctrl      : std_logic_vector( 3 downto 0); 
  
  signal enc_bypass    : std_logic; -- from external controller (e.g. VIO), must be synced with gt_tx_usrclk
  signal disp_from_enc : std_logic;  
  signal disp_to_enc   : std_logic;  
  
  signal gt_ctrl       : gt_ctrl_t:= GT_CTRL_NULL;
  signal gt_stat       : gt_stat_t;
  signal to_gt         : to_gt_t;
  signal from_gt       : from_gt_t;
  
  --signals for data checker at receiver
  signal resetCheck    : std_logic;
  signal valid         : std_logic;  
  signal data_check    : std_logic_vector(31 downto 0);
  signal detectComma   : std_logic;
  --
  
  signal dummy         : std_logic := '0';
  
  signal alignerSlide  : std_logic := '0';
  signal vioSlide      : std_logic := '0';
  signal singleVIOSlide: std_logic := '0'; 
  signal autoAlign     : std_logic := '1'; 
  signal slideCount    : std_logic_vector(14 downto 0); 
  signal aligned       : std_logic;
   
  --signal rxUserClk     : std_logic;
  --signal txUserClk     : std_logic;
  
  signal st : std_logic_vector(1 downto 0); 
  
begin

  --SFP1_enable <= '1'; --not necessary: SFP1_tx_disable is forced at ground by jumper J6
  
  gt_ctrl.rxslide <= alignerSlide or singleVIOSlide;
  
  clk_inst: entity work.clk_buf
  port map
  (
    i     => clk_125_p,  
    ib    => clk_125_n,  
    o     => clk_sys
  );
  
  gen_inst: entity work.data_generator
    port map(
      rst_i   => gen_rst, -- from VIO
      clk_i   => from_gt.tx_usrclk,
      data_o  => gen_data,
      ctrl_o  => gen_ctrl
    );

  enc_inst: entity work.multibyte_enc8b10b
    port map(
      clk_i         => from_gt.tx_usrclk,
      ctrl_i        => gen_ctrl,
      data_i        => gen_data,
      rundp_o       => disp_from_enc,
      rundp_i       => disp_to_enc,
      bypass_en_i   => enc_bypass,
      bypass_data_i => x"FFFFF00000",
      data_o        => to_gt.tx_data
    );
      disp_to_enc <= disp_from_enc;
      
  ila_tx_inst: entity work.ila_1
    port map(
      clk           => from_gt.tx_usrclk,
      probe0        => gen_data
  );

  gt_inst: entity work.m_gt
    port map(
      clk_sys      => clk_sys,
      gt_refclk_p  => gt_refclk_p,
      gt_refclk_n  => gt_refclk_n,
      gt_rx_p(0)   => gt_rx_p,  
      gt_rx_n(0)   => gt_rx_n,  
      gt_tx_p(0)   => gt_tx_p,  
      gt_tx_n(0)   => gt_tx_n,  
      gt_ctrl_i(0) => gt_ctrl, --> record containing signals that could be driven  by an external controller (e.g. VIO)  
      gt_stat_o(0) => gt_stat, --> record containing signals that could be checked by an external controller (e.g. VIO)
      gt_i(0)      => to_gt,   --> record containing signals from the user logic to the GT 
      gt_o(0)      => from_gt  --> record containing signals from the GT to the user logic
    );
    
-- aligner_inst: entity work.aligner
--  port map(
--    clk            => from_gt.rx_usrclk,
--    rst            => not(autoAlign and from_gt.rx_rdy), -- form VIO
--    byteIsAligned  => from_gt.rx_aligned,
--    rxSlide        => alignerSlide,
--    slideCount     => slideCount
--  );
  
 fsm_aligner_inst: entity work.fsm_aligner
  port map(
    clk            => from_gt.rx_usrclk,
    rst            => not(autoAlign and from_gt.rx_rdy), -- autoAlign form VIO
    rxdata         => from_gt.rx_data,
    ctrl           => from_gt.rx_ctrl2,
    rxSlide        => alignerSlide,
    slideCount     => slideCount,
    byteAligned    => aligned,  
     
    st   => st
  );

 mono_inst: entity work.hysteresis_monostable
  port map(
    rst                      => '0',
    clk                      => from_gt.rx_usrclk,
    monostable_trigger_async => vioSlide,
    monostable_out           => singleVIOSlide
  );
  
 rxuserclk_1_buf_inst: entity work.oddr
  port map(
    clk_in  => from_gt.rx_usrclk,
    clk_out => rxUserClk_1
    --clk_in  => from_gt.tx_usrclk,
    --clk_out => txUserClk
  );
  
 rxuserclk_2_buf_inst: entity work.oddr
  port map(
    clk_in  => from_gt.rx_usrclk,
    clk_out => rxUserClk_2
    --clk_in  => from_gt.tx_usrclk,
    --clk_out => txUserClk
  );
  
-- OBUFDS_inst : OBUFDS
--   port map (
--      --O  => rxUserClk_p,  -- 1-bit output: Diff_p output (connect directly to top-level port)
--      --OB => rxUserClk_n, -- 1-bit output: Diff_n output (connect directly to top-level port)
--      --I  => rxUserClk    -- 1-bit input: Buffer input
--      O  => txUserClk_p,  -- 1-bit output: Diff_p output (connect directly to top-level port)
--      OB => txUserClk_n, -- 1-bit output: Diff_n output (connect directly to top-level port)
--      I  => txUserClk    -- 1-bit input: Buffer input
--   );
    
  resetCheck <= not aligned; --from_gt.rx_aligned; 
    
  check_inst: entity work.data_checker
    port map(
      rst_i     => resetCheck,
      clk_i     => from_gt.rx_usrclk,
      comDet    => from_gt.rx_ctrl2(0),
      aligned   => aligned,  --from_gt.rx_aligned;
      data_in   => from_gt.rx_data,
      valid     => valid,
      data_gen  => data_check
    );

  -------------------------------------
  -- DEBUGGING TOOLS BELOW
  -------------------------------------  
  vio_tx_inst: entity work.vio_0
    port map(
      clk           => from_gt.tx_usrclk,
      probe_in0(0)  => dummy,
      probe_out0(0) => gen_rst,
      probe_out1(0) => enc_bypass,
      probe_out2(0) => gt_ctrl.txpolarity
    );
    
  vio_sys_inst: entity work.vio_0
    port map(
      clk           => clk_sys,
      probe_in0(0)  => dummy,
      probe_out0(0) => gt_ctrl.gtwiz_reset_all,
      probe_out1(0) => gt_ctrl.gtwiz_reset_tx_pll_and_datapath,
      probe_out2(0) => gt_ctrl.gtwiz_reset_rx_pll_and_datapath
    );
    
  vio_rx_inst: entity work.vio_1
    port map(
      clk           => from_gt.rx_usrclk,
      probe_in0(0)  => from_gt.rx_rdy,
      probe_out0(0) => vioSlide,
      probe_out1(0) => autoAlign  -- active high
    );            
        
  ila_rx_inst: entity work.ila_0
    port map(
      clk           => from_gt.rx_usrclk,
      probe0        => from_gt.rx_data,
      probe1        => from_gt.rx_ctrl0,-- rxbyte is a K character
      probe2(0)     => '0',
      probe3(0)     => valid,
      probe4        => data_check,
      probe5(0)     => aligned, --from_gt.rx_aligned,
      probe6(0)     => gt_ctrl.rxslide,
      probe7        => from_gt.rx_ctrl2,
      probe8        => slideCount,
      
      probe9    => st
  );
        
end architecture;
