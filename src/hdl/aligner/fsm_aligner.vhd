----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Edoardo Orzes
-- 
-- Create Date: 02/13/2023 02:25:31 PM
-- Module Name: aligner - Behavioral
-- Target Devices: GTH Kintex Ultrascale Transceiver
-- Description: 

--  FSM automated byte aliner for manual alinment mode.
--  The comma that it's searched is k28.5 (x"BC") in the least significant byte on the 4 bytes bus.
--  To change this edit the lines 109 and 110, after when ALIGNED =>

-- Revision: v1.0
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;


entity fsm_aligner is
  generic (
    CYCLES_BETWEEN_COMMAS       : integer := 5; -- 5 rxusrclk cycles is the default window for searching for comma in SEARCH state before going to SLIDE or ALIGNED state.
    ERRORS_THRESHOLD_TO_REALIGN : integer := 16 -- 16 consecutive errors is the default threshold that triggers to SLIDE state from the ALIGNED state.
  ); 
  port( 
    clk            : in  std_logic;
    rst            : in  std_logic;
    rxdata         : in  std_logic_vector(31 downto 0);
    ctrl           : in  std_logic_vector(3 downto 0);   -- control for special symbols in 8b10b
    rxSlide        : out std_logic;                      -- output for the transceiver
    slideCount     : out std_logic_vector(14 downto 0);
    byteAligned    : out std_logic;
    
    st : out std_logic_vector(1 downto 0) -- debug for ILA
  );
end fsm_aligner;

architecture Behavioral of fsm_aligner is

    constant CYCLES_TO_WAIT_BETWEEN_SLIDES : integer := 35;      -- minimum time between assertions of rxslide is 32 rxusrclk cycles
    constant TDLOCK                        : integer := 2300000; -- TDLOCK (CDR to data max time for LPM equaliser) is the time to wait after the reset before clk is in place
    
    -- COUNTERS --
    signal count      : integer range 0 to 50;      -- > CYCLES_TO_WAIT_BETWEEN_SLIDES
    signal count_s    : integer range 0 to 1000;    -- > 4*CYCLES_BETWEEN_COMMAS
    signal n_slide    : integer range 0 to 32768;   -- > 15 bits of st signal
    signal count_r    : integer range 0 to 3000000; -- > TDLOCK
    signal errorCount : integer range 0 to 1000;    -- > ERRORS_THRESHOLD_TO_REALIGN
    
    -- FSM STATES --
    type state_t is (STOP, SEARCH, SLIDE, ALIGNED);
    attribute enum_enc: string;
    attribute enum_enc of state_t: type is "00 01 10 11";
    signal state     : state_t;
    signal intState  : integer := 0;
    
begin

    intState <= state_t'POS(state);
    st <= std_logic_vector(to_unsigned(intState, 2));

    slideCount <= std_logic_vector(to_unsigned(n_slide, 15));

    FSM : process(rst, clk)
        begin
            if rst = '1' then
                state <= STOP;
                count <= 0;
                count_s <= 0;
                count_r <= 0;
            elsif rising_edge(clk) then
            
                case(state) is 
                    when STOP =>
                        if count_r < TDLOCK then 
                            count_r <= count_r + 1;
                        else 
                            count_r <= 0;
                            state <= SEARCH;
                        end if;
                    
                    when SEARCH => 
                        if count_s < 4*CYCLES_BETWEEN_COMMAS then
                            count_s <= count_s + 1;
                        else 
                            count_s <= 0;
                            state <= SLIDE;
                        end if;
                        if ctrl = "0001" then
                            count_s <= 0;
                            if rxdata(7 downto 0) = x"BC" then
                                state <= ALIGNED;
                            else 
                                state <= SLIDE;
                            end if;
                        end if;
                        
                    when SLIDE =>
                        if count < CYCLES_TO_WAIT_BETWEEN_SLIDES then  
                            count <= count + 1;
                        else 
                            count <= 0;
                            state <= SEARCH;
                        end if;
                        
                    when ALIGNED => 
                        if ctrl = "0001" then
                            if rxdata(7 downto 0) = x"BC" then
                                if errorCount > 0 then
                                    errorCount <= errorCount - 1;
                                end if;
                            else 
                                errorCount <= errorCount + 1;
                            end if;
                        end if;
                        if errorCount > ERRORS_THRESHOLD_TO_REALIGN - 1 then
                            errorCount <= 0;
                            state <= SLIDE;
                        end if;
                        
                end case;
            end if;
        end process;
        
     FSM_outputs: process(state, count) begin
        case(state) is
            when STOP => 
                byteAligned <= '0';
            
            when SEARCH => 
                byteAligned <= '0';

            when SLIDE => 
                byteAligned <= '0';
                if count = 1 or count = 2 then
                    rxSlide <= '1';
                else
                    rxSlide <= '0';
                end if;
                
            when ALIGNED =>
                byteAligned <= '1';
                
        end case;
     end process;
     
     process(rst, clk) begin
         if rst = '1' then
            n_slide <= 0;
         elsif rising_edge(clk) then
            if count = 2 then 
                n_slide <= n_slide + 1;
            end if;
         end if;
     end process;

end Behavioral;


