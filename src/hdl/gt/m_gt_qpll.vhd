library ieee;
use ieee.std_logic_1164.all;

entity m_gt_qpll is
  port(
    sysclk_i    : in  std_logic;
    refclk_i    : in  std_logic;
    reset_i     : in  std_logic;
    lock_o      : out std_logic;
    outclk_o    : out std_logic;
    outrefclk_o : out std_logic
  );
end m_gt_qpll;

architecture rtl of m_gt_qpll is
  
  component m_gth_gthe3_common_wrapper is
    port
    (
      gthe3_common_bgbypassb            : in  std_logic_vector( 0 downto 0);
      gthe3_common_bgmonitorenb         : in  std_logic_vector( 0 downto 0);
      gthe3_common_bgpdb                : in  std_logic_vector( 0 downto 0);
      gthe3_common_bgrcalovrd           : in  std_logic_vector( 4 downto 0);
      gthe3_common_bgrcalovrdenb        : in  std_logic_vector( 0 downto 0);
      gthe3_common_drpaddr              : in  std_logic_vector( 8 downto 0);
      gthe3_common_drpclk               : in  std_logic_vector( 0 downto 0);
      gthe3_common_drpdi                : in  std_logic_vector(15 downto 0);
      gthe3_common_drpen                : in  std_logic_vector( 0 downto 0);
      gthe3_common_drpwe                : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtgrefclk0           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtgrefclk1           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk00      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk01      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk10      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk11      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk00           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk01           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk10           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk11           : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk00      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk01      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk10      : in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk11      : in  std_logic_vector( 0 downto 0);
      gthe3_common_pmarsvd0             : in  std_logic_vector( 7 downto 0);
      gthe3_common_pmarsvd1             : in  std_logic_vector( 7 downto 0);
      gthe3_common_qpll0clkrsvd0        : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0clkrsvd1        : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0lockdetclk      : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0locken          : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0pd              : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0refclksel       : in  std_logic_vector( 2 downto 0);
      gthe3_common_qpll0reset           : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1clkrsvd0        : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1clkrsvd1        : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1lockdetclk      : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1locken          : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1pd              : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1refclksel       : in  std_logic_vector( 2 downto 0);
      gthe3_common_qpll1reset           : in  std_logic_vector( 0 downto 0);
      gthe3_common_qpllrsvd1            : in  std_logic_vector( 7 downto 0);
      gthe3_common_qpllrsvd2            : in  std_logic_vector( 4 downto 0);
      gthe3_common_qpllrsvd3            : in  std_logic_vector( 4 downto 0);
      gthe3_common_qpllrsvd4            : in  std_logic_vector( 7 downto 0);
      gthe3_common_rcalenb              : in  std_logic_vector( 0 downto 0);
      gthe3_common_drpdo                : out std_logic_vector(15 downto 0);
      gthe3_common_drprdy               : out std_logic_vector( 0 downto 0);
      gthe3_common_pmarsvdout0          : out std_logic_vector( 7 downto 0);
      gthe3_common_pmarsvdout1          : out std_logic_vector( 7 downto 0);
      gthe3_common_qpll0fbclklost       : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0lock            : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0outclk          : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0outrefclk       : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0refclklost      : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1fbclklost       : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1lock            : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1outclk          : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1outrefclk       : out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1refclklost      : out std_logic_vector( 0 downto 0);
      gthe3_common_qplldmonitor0        : out std_logic_vector( 7 downto 0);
      gthe3_common_qplldmonitor1        : out std_logic_vector( 7 downto 0);
      gthe3_common_refclkoutmonitor0    : out std_logic_vector( 0 downto 0);
      gthe3_common_refclkoutmonitor1    : out std_logic_vector( 0 downto 0);
      gthe3_common_rxrecclk0_sel        : out std_logic_vector( 1 downto 0);
      gthe3_common_rxrecclk1_sel        : out std_logic_vector( 1 downto 0)
    );
  end component;
 
begin
  
  inst: m_gth_gthe3_common_wrapper -- constants extracted from m_gth_ex/imports/m_gth_example_wrapper.v
    port map(
      gthe3_common_bgbypassb            => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_bgmonitorenb         => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_bgpdb                => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_bgrcalovrd           => 5b"1"       , --: in  std_logic_vector( 4 downto 0);
      gthe3_common_bgrcalovrdenb        => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_drpaddr              => 9b"0"       , --: in  std_logic_vector( 8 downto 0);
      gthe3_common_drpclk               => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_drpdi                => 16b"0"      , --: in  std_logic_vector(15 downto 0);
      gthe3_common_drpen                => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_drpwe                => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtgrefclk0           => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtgrefclk1           => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk00      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk01      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk10      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtnorthrefclk11      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk00           => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk01(0)        => refclk_i    , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk10           => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtrefclk11           => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk00      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk01      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk10      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_gtsouthrefclk11      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_pmarsvd0             => 8b"0"       , --: in  std_logic_vector( 7 downto 0);
      gthe3_common_pmarsvd1             => 8b"0"       , --: in  std_logic_vector( 7 downto 0);
      gthe3_common_qpll0clkrsvd0        => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0clkrsvd1        => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0lockdetclk      => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0locken          => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0pd              => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll0refclksel       => "001"       , --: in  std_logic_vector( 2 downto 0);
      gthe3_common_qpll0reset           => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1clkrsvd0        => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1clkrsvd1        => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1lockdetclk(0)   => sysclk_i    , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1locken          => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1pd              => 1b"0"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpll1refclksel       => "001"       , --: in  std_logic_vector( 2 downto 0);
      gthe3_common_qpll1reset(0)        => reset_i     , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_qpllrsvd1            => 8b"0"       , --: in  std_logic_vector( 7 downto 0);
      gthe3_common_qpllrsvd2            => 5b"0"       , --: in  std_logic_vector( 4 downto 0);
      gthe3_common_qpllrsvd3            => 5b"0"       , --: in  std_logic_vector( 4 downto 0);
      gthe3_common_qpllrsvd4            => 8b"0"       , --: in  std_logic_vector( 7 downto 0);
      gthe3_common_rcalenb              => 1b"1"       , --: in  std_logic_vector( 0 downto 0);
      gthe3_common_drpdo                => open        , --: out std_logic_vector(15 downto 0);
      gthe3_common_drprdy               => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_pmarsvdout0          => open        , --: out std_logic_vector( 7 downto 0);
      gthe3_common_pmarsvdout1          => open        , --: out std_logic_vector( 7 downto 0);
      gthe3_common_qpll0fbclklost       => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0lock            => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0outclk          => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0outrefclk       => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll0refclklost      => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1fbclklost       => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1lock(0)         => lock_o      , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1outclk(0)       => outclk_o    , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1outrefclk(0)    => outrefclk_o , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qpll1refclklost      => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_qplldmonitor0        => open        , --: out std_logic_vector( 7 downto 0);
      gthe3_common_qplldmonitor1        => open        , --: out std_logic_vector( 7 downto 0);
      gthe3_common_refclkoutmonitor0    => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_refclkoutmonitor1    => open        , --: out std_logic_vector( 0 downto 0);
      gthe3_common_rxrecclk0_sel        => open        , --: out std_logic_vector( 1 downto 0);
      gthe3_common_rxrecclk1_sel        => open          --: out std_logic_vector( 1 downto 0)
    );

end rtl;