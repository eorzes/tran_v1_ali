`timescale 1ps/1ps

// =====================================================================================================================
// This example design wrapper module instantiates the core and any helper blocks which the user chose to exclude from
// the core, connects them as appropriate, and maps enabled ports
// =====================================================================================================================


module m_gt_qpll (
  input  wire sysclk_i
 ,input  wire refclk_i
 ,input  wire reset_i
 ,output wire lock_o
 ,output wire outclk_o
 ,output wire outrefclk_o
);
      // instantiation taken from m_gth_ex/imports/m_gth_example_wrapper.v
      m_gth_gthe3_common_wrapper gthe3_common_wrapper_inst (
        .GTHE3_COMMON_BGBYPASSB         (1'b1),
        .GTHE3_COMMON_BGMONITORENB      (1'b1),
        .GTHE3_COMMON_BGPDB             (1'b1),
        .GTHE3_COMMON_BGRCALOVRD        (5'b1),
        .GTHE3_COMMON_BGRCALOVRDENB     (1'b1),
        .GTHE3_COMMON_DRPADDR           (9'b0),
        .GTHE3_COMMON_DRPCLK            (1'b0),
        .GTHE3_COMMON_DRPDI             (16'b0),
        .GTHE3_COMMON_DRPEN             (1'b0),
        .GTHE3_COMMON_DRPWE             (1'B0),
        .GTHE3_COMMON_GTGREFCLK0        (1'b0),
        .GTHE3_COMMON_GTGREFCLK1        (1'b0),
        .GTHE3_COMMON_GTNORTHREFCLK00   (1'b0),
        .GTHE3_COMMON_GTNORTHREFCLK01   (1'b0),
        .GTHE3_COMMON_GTNORTHREFCLK10   (1'b0),
        .GTHE3_COMMON_GTNORTHREFCLK11   (1'b0),
        .GTHE3_COMMON_GTREFCLK00        (1'b0),
        .GTHE3_COMMON_GTREFCLK01        (refclk_i),
        .GTHE3_COMMON_GTREFCLK10        (1'b0),
        .GTHE3_COMMON_GTREFCLK11        (1'b0),
        .GTHE3_COMMON_GTSOUTHREFCLK00   (1'b0),
        .GTHE3_COMMON_GTSOUTHREFCLK01   (1'b0),
        .GTHE3_COMMON_GTSOUTHREFCLK10   (1'b0),
        .GTHE3_COMMON_GTSOUTHREFCLK11   (1'b0),
        .GTHE3_COMMON_PMARSVD0          (8'b0),
        .GTHE3_COMMON_PMARSVD1          (8'b0),
        .GTHE3_COMMON_QPLL0CLKRSVD0     (1'b0),
        .GTHE3_COMMON_QPLL0CLKRSVD1     (1'b0),
        .GTHE3_COMMON_QPLL0LOCKDETCLK   (1'b0),
        .GTHE3_COMMON_QPLL0LOCKEN       (1'b0),
        .GTHE3_COMMON_QPLL0PD           (1'b1),
        .GTHE3_COMMON_QPLL0REFCLKSEL    (3'b001),
        .GTHE3_COMMON_QPLL0RESET        (1'b1),
        .GTHE3_COMMON_QPLL1CLKRSVD0     (1'b0),
        .GTHE3_COMMON_QPLL1CLKRSVD1     (1'b0),
        .GTHE3_COMMON_QPLL1LOCKDETCLK   (sysclk_i),
        .GTHE3_COMMON_QPLL1LOCKEN       (1'b1),
        .GTHE3_COMMON_QPLL1PD           (1'b0),
        .GTHE3_COMMON_QPLL1REFCLKSEL    (3'b001),
        .GTHE3_COMMON_QPLL1RESET        (reset_i),
        .GTHE3_COMMON_QPLLRSVD1         (8'b0),
        .GTHE3_COMMON_QPLLRSVD2         (5'b0),
        .GTHE3_COMMON_QPLLRSVD3         (5'b0),
        .GTHE3_COMMON_QPLLRSVD4         (8'b0),
        .GTHE3_COMMON_RCALENB           (1'b1),
        .GTHE3_COMMON_DRPDO             (),
        .GTHE3_COMMON_DRPRDY            (),
        .GTHE3_COMMON_PMARSVDOUT0       (),
        .GTHE3_COMMON_PMARSVDOUT1       (),
        .GTHE3_COMMON_QPLL0FBCLKLOST    (),
        .GTHE3_COMMON_QPLL0LOCK         (),
        .GTHE3_COMMON_QPLL0OUTCLK       (),
        .GTHE3_COMMON_QPLL0OUTREFCLK    (),
        .GTHE3_COMMON_QPLL0REFCLKLOST   (),
        .GTHE3_COMMON_QPLL1FBCLKLOST    (),
        .GTHE3_COMMON_QPLL1LOCK         (lock_o),
        .GTHE3_COMMON_QPLL1OUTCLK       (outclk_o),
        .GTHE3_COMMON_QPLL1OUTREFCLK    (outrefclk_o),
        .GTHE3_COMMON_QPLL1REFCLKLOST   (),
        .GTHE3_COMMON_QPLLDMONITOR0     (),
        .GTHE3_COMMON_QPLLDMONITOR1     (),
        .GTHE3_COMMON_REFCLKOUTMONITOR0 (),
        .GTHE3_COMMON_REFCLKOUTMONITOR1 (),
        .GTHE3_COMMON_RXRECCLK0_SEL     (),
        .GTHE3_COMMON_RXRECCLK1_SEL     ()
      );
  
endmodule
